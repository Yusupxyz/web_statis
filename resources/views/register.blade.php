<!DOCTYPE html>
<html>
<head>
<title>Sanber Book</title>
</head>
<body>
<h1>Buat Account Baru!</h1>
<h3>Sign Up Form</h3>
<form action="{{ url('welcome') }}" method="post">
@csrf
    <label for="fname">First name:</label><br><br>
    <input type="text" name="fname" required><br><br>

    <label for="lname">Last name:</label><br><br>
    <input type="text" name="lname" required><br><br>

    <label for="gender">Gender:</label><br><br>
    <input type="radio" name="gender" value="male" required>
    <label for="html">Male</label><br>
    <input type="radio" name="gender" value="female">
    <label for="css">Female</label><br>
    <input type="radio" name="gender" value="other">
    <label for="javascript">Other</label><br><br>

    <label for="nationality">Nationality:</label><br><br>
    <select name="nationality" required>
        <option value="indonesian">Indonesian</option>
        <option value="singapore">Singapore</option>
        <option value="malaysian">Malaysian</option>
        <option value="australian">Australian</option>
      </select><br><br>

    <label for="language">Language Spoken:</label><br><br>
    <input type="checkbox" name="language[]" value="indonesia" required>
    <label for="language"> Bahasa Indonesia</label><br>
    <input type="checkbox" name="language[]" value="english">
    <label for="language"> English</label><br>
    <input type="checkbox" name="language[]" value="other">
    <label for="language"> Other</label><br><br>

    <label for="bio">Bio:</label><br><br>
    <textarea name="bio" rows="6" cols="30" required></textarea><br><br>

    <input type="submit" value="Submit">

  </form> 
</body>
</html>